#include "functions.h"

int diagonalSum(QVector<QVector<int>> vector)    //сумма элементов главной и побочной диагоналей
{
    int sum = 0;

    for (int i = 0; i < vector.size(); i++)
    {
        if (i < vector[i].size())
        {
            sum += vector[i][i];
        }
        if (i != (vector[i].size() - i - 1) && (vector[i].size() - i - 1) >= 0)
        {
            sum += vector[i][vector[i].size() - i - 1];
        }
    }

    return sum;
}
