#include "functions.h"

int evenSum(QVector<QVector<int>> vector)    //cумма элементов, у которых сумма индексов чётная
{
    int sum = 0;

    for (int i = 0; i < vector.size(); i++)
    {
        for (int j = 0; j < vector[i].size(); j++)
        {
            if ((i + j) % 2 == 0)
            {
                sum += vector[i][j];
            }
        }
    }

    return sum;
}
