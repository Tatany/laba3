/**
*\file
*\brief Программа для работы с двумерными массивами
*\author Татьяна
*\version 1.0
*\date 08.06.2019
*\example
*
*Все необходимые функции для обработки результатов
*/
/**
*\brief Функция возвращает позиции равных элементов двух двумерных массивов
*\param[in] Двумерный массив
*\param[in] Двумерный массив, с которым сравнивают
*\return Позиции равных элементов
*/
QVector<QVector<int>> compare(QVector<QVector<int>> mainVector, QVector<QVector<int>> addVector);
/**
*\brief Функция возвращает позицию элемента двумерного массива, то есть номер строки и столбца элемента
*\param[in] Двумерный массив
*\param[in] Значение элемента, который необходимо найти
*\return Позиция элемента
*/
QVector<QVector<int>> findElement(QVector<QVector<int>> vector, int element);
/**
*\brief Функция возвращает позиции трёх наибольших элементов двумерного массива
*\param[in] Двумерный массив
*\return Позиции элементов
*/
QVector<QVector<int>> threeMaxElements(QVector<QVector<int>> vector);
/**
*\brief Функция возвращает сумму элементов главной и побочной диагоналей двумерного массива
*\param[in] Двумерный массив
*\return Сумма элементов
*/
int diagonalSum(QVector<QVector<int>> vector);
/**
*\brief Функция возвращает сумму элементов двумерного массива, у которых сумма индексов чётная
*\param[in] Двумерный массив
*\return Сумма элементов
*/
int evenSum(QVector<QVector<int>> vector);
/**
*\brief Функция возвращает двумерный массив, отраженный по вертикали
*\param[in] Двумерный массив
*\return Думерный массив, отраженный по вертикали
*/
QVector<QVector<int>> reflection(QVector<QVector<int>> vector);
/**
*\brief Функция возвращает двумерный массив, отсортированный по возрастанию значений элементов массива
*\param[in] Двумерный массив
*\return Отсортированный двумерный массив
*/
QVector<QVector<int>> sortInAscendingOrder(QVector<QVector<int>> vector);
/**
*\brief Функция возвращает двумерный массив, отсортированный по убыванию значений элементов массива
*\param[in] Двумерный массив
*\return Отсортированный двумерный массив
*/
QVector<QVector<int>> sortInDescendingOrder(QVector<QVector<int>> vector);
