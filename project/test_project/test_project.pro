QT += testlib
QT -= gui

TARGET = tst_test_project
CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    main.cpp \
    tst_test_project.cpp \

SOURCES += \
    ../compare.cpp \
    ../findelement.cpp \
    ../threemaxelements.cpp \
    ../diagonalsum.cpp \
    ../evensum.cpp \
    ../reflection.cpp \
    ../sortinascendingorder.cpp \
    ../sortindescendingorder.cpp

HEADERS += \
    tst_test_project.h

HEADERS += ../functions.h
