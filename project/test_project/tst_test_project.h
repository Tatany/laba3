#ifndef TST_TEST_PROJECT_H
#define TST_TEST_PROJECT_H

#include <QtTest/QtTest>

class test_project : public QObject
{
    Q_OBJECT

public:
    test_project();

private slots:

    void test_compare();

    void test_findElement();

    void test_threeMaxElements();

    void test_diagonalSum();

    void test_evenSum();

    void test_reflection();

    void test_sortInAscendingOrder();

    void test_sortInDescendingOrder();

};

#endif // TST_TEST_PROJECT_H
