#include <QtTest>
#include <../functions.h>
#include "tst_test_project.h"

test_project::test_project()
{

}

bool compareVectors(const QVector<QVector<int>> &vector1, const QVector<QVector<int>> &vector2) {
    if (vector1 == vector2)    //сравнение векторов
        return true;
    return false;
}

//пак тестов для функции compare - сравнение двух массивов
void test_project::test_compare()
{
    QVector<QVector<int>> test_mainVector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    QVector<QVector<int>> test_addVector = {{10, 20, 30}, {450, 20, -4290}, {-807, 520, 50}};
    QVector<QVector<int>> test_sameElements(0);
    QCOMPARE(true, compareVectors(compare(test_mainVector, test_addVector), test_sameElements));

    test_mainVector = {{71, 52, 3}, {45, 2, -429}, {-807, 52, 0}};
    test_addVector = {{17, 72, 3}, {45, 20, -49}, {-87, 552, 0}};
    test_sameElements = {{1, 3}, {2, 1}, {3, 3}};
    QCOMPARE(true, compareVectors(compare(test_mainVector, test_addVector), test_sameElements));

    test_mainVector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    test_addVector = {{1, 2, 34}, {45, 20, -429}, {-87, 52, 10}};
    test_sameElements = {{1, 3}, {2, 2}, {3, 3}};
    QCOMPARE(false, compareVectors(compare(test_mainVector, test_addVector), test_sameElements));
}

//пак тестов для функции findElement - поиск элемента
void test_project::test_findElement()
{
    QVector<QVector<int>> test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    QVector<QVector<int>> test_elementIndex = {{1, 2}, {2, 2}};
    QCOMPARE(true, compareVectors(findElement(test_vector, 2), test_elementIndex));

    test_vector = {{1, 2, 3}, {45, -429, 2}, {-87, 52, 0}};
    test_elementIndex = {{0, 1}, {1, 1}};
    QCOMPARE(false, compareVectors(findElement(test_vector, 2), test_elementIndex));
}

//пак тестов для функции threeMaxElements - поиск трех наибольших элементова
void test_project::test_threeMaxElements()
{
    QVector<QVector<int>> test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    QVector<QVector<int>> test_threeMax = {{52}, {3, 2}, {45}, {2, 1}, {3}, {1, 3}};
    QCOMPARE(true, compareVectors(threeMaxElements(test_vector), test_threeMax));

    test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    test_threeMax = {{52}, {3, 2}, {45}, {2, 1}, {45}, {1, 3}};
    QCOMPARE(false, compareVectors(threeMaxElements(test_vector), test_threeMax));
}

//пак тестов для функции diagonalSum - сумма элементов главной и побочной диагоналей
void test_project::test_diagonalSum()
{
    QVector<QVector<int>> test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    QCOMPARE(diagonalSum(test_vector), -81);

    test_vector = {{1, 123, 3}, {-87, 52, 0}};
    QCOMPARE(diagonalSum(test_vector), 56);

    test_vector = {{1, 3}, {2, -429}, {57, 0}};
    QCOMPARE(diagonalSum(test_vector), -423);
}

//пак тестов для функции evenSum - cумма элементов, у которых сумма индексов чётная
void test_project::test_evenSum()
{
    QVector<QVector<int>> test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    QCOMPARE(evenSum(test_vector), -81);

    test_vector = {{1, 123, 3, -9}, {-87, 52, 0, 64}};
    QCOMPARE(evenSum(test_vector), 120);
}

//пак тестов для функции reflection - отражение по вертикали
void test_project::test_reflection()
{
    QVector<QVector<int>> test_vector_before = {{1, 2, 3}, {45, -2, 429}, {-87, 52, 0}};
    QVector<QVector<int>> test_vector_after = {{-87, 52, 0}, {45, -2, 429}, {1, 2, 3}};
    QCOMPARE(true, compareVectors(reflection(test_vector_before), test_vector_after));

    test_vector_before = {{1, 2, 3}, {45, -2, 429}, {-87, 52, 0}};
    test_vector_after = {{1, 2, 3}, {-87, 52, 0}, {45, -2, 429}};
    QCOMPARE(false, compareVectors(reflection(test_vector_before), test_vector_after));
}
//пак тестов для функции sortInAscendingOrder - сортировки по возрастанию
void test_project::test_sortInAscendingOrder()
{
    QVector<QVector<int>> test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    QVector<QVector<int>> test_sortVector = {{-429, -87, 0}, {1, 2, 2}, {3, 45, 52}};
    QCOMPARE(true, compareVectors(sortInAscendingOrder(test_vector), test_sortVector));

    test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    test_sortVector = {{-429, -87, 0}, {1, 2, 3}, {2, 45, 52}};
    QCOMPARE(false, compareVectors(sortInAscendingOrder(test_vector), test_sortVector));
}

//пак тестов для функции sortInDescendingOrder - сортировки по убыванию
void test_project::test_sortInDescendingOrder()
{
    QVector<QVector<int>> test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    QVector<QVector<int>> test_sortVector = {{52, 45, 3}, {2, 2, 1}, {0, -87, -429}};
    QCOMPARE(true, compareVectors(sortInDescendingOrder(test_vector), test_sortVector));

    test_vector = {{1, 2, 3}, {45, 2, -429}, {-87, 52, 0}};
    test_sortVector = {{-429, -87, 0}, {1, 2, 2}, {3, 45, 52}};
    QCOMPARE(false, compareVectors(sortInDescendingOrder(test_vector), test_sortVector));
}
