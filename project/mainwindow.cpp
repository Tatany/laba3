#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functions.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QVector<QVector<int>> MainWindow::fillVector()    //заполнение массива
{
    bool flag = true;    //проверка на пустые ячейки

    if (ui->tableWidget->rowCount() == 0)
    {
        QMessageBox::critical(nullptr, QObject::tr("Ошибка"), ("Пустой массив  "));
        return QVector<QVector<int>>(0);
    }
    else
    {
        for (int i = 0; i < ui->tableWidget->rowCount(); i++)
        {
            for (int j = 0; j < ui->tableWidget->columnCount(); j++)
            {
                if (ui->tableWidget->item(i, j) == nullptr)
                {
                    flag = false;
                    break;
                }
            }
        }
    }

    if (flag)
    {
        QVector<QVector<int>> vector;
        for (int i = 0; i < ui->tableWidget->rowCount(); i++)    //заполнение массива
        {
            QVector<int> tmp;
            for (int j = 0; j < ui->tableWidget->columnCount(); j++)
            {
                tmp.append(ui->tableWidget->item(i, j)->data(Qt::EditRole).toInt());
            }
            vector.append(tmp);
        }
        return vector;
    }
    else
    {
        QMessageBox::warning(nullptr, QObject::tr("Ошибка"), ("Есть пустые элементы  "));
        return QVector<QVector<int>>(0);
    }
}

QVector<QVector<int>> MainWindow::fillAddVector()    //заполнение массива
{
    bool flag = true;    //проверка на пустые ячейки

    if (ui->tableWidget_2->rowCount() == 0)
    {
        QMessageBox::critical(nullptr, QObject::tr("Ошибка"), ("Пустой массив  "));
        return QVector<QVector<int>>(0);
    }
    else
    {
        for (int i = 0; i < ui->tableWidget_2->rowCount(); i++)
        {
            for (int j = 0; j < ui->tableWidget_2->columnCount(); j++)
            {
                if (ui->tableWidget_2->item(i, j) == nullptr)
                {
                    flag = false;
                    break;
                }
            }
        }
    }

    if (flag)
    {
        QVector<QVector<int>> addVector;
        for (int i = 0; i < ui->tableWidget_2->rowCount(); i++)    //заполнение массива
        {
            QVector<int> tmp;
            for (int j = 0; j < ui->tableWidget_2->columnCount(); j++)
            {
                tmp.append(ui->tableWidget_2->item(i, j)->data(Qt::EditRole).toInt());
            }
            addVector.append(tmp);
        }
        return addVector;
    }
    else
    {
        QMessageBox::warning(nullptr, QObject::tr("Ошибка"), ("Есть пустые элементы  "));
        return QVector<QVector<int>>(0);
    }
}

void MainWindow::on_pushButton_clicked()    //создание таблицы
{
    ui->tableWidget->setItemDelegate(new SpinBoxDelegate);
    ui->tableWidget->setRowCount(ui->spinBox->value());
    ui->tableWidget->setColumnCount(ui->spinBox_2->value());
    ui->tableWidget->resizeColumnsToContents();
}

void MainWindow::on_pushButton_2_clicked()    //сброс таблицы
{
    for (int i = 0; i < ui->tableWidget->rowCount(); i++)    //очистка таблицы
    {
        ui->tableWidget->removeRow(i);
    }
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(0);
}

void MainWindow::on_pushButton_3_clicked()    //заполнение случайными числами
{
    if (ui->tableWidget->rowCount() == 0)
    {
        QMessageBox::critical(nullptr, QObject::tr("Ошибка"), ("Пустой массив  "));
    }
    else
    {
        for (int i = 0; i < ui->tableWidget->rowCount(); i++)
        {
            for (int j = 0; j < ui->tableWidget->columnCount(); j++)
            {
                QTableWidgetItem* item = new QTableWidgetItem();
                item->setData(Qt::EditRole, rand() % 2001 - 1000);
                ui->tableWidget->setItem(i, j, item);
            }
        }
    }
}

void MainWindow::on_pushButton_4_clicked()    //отражение по вертикали
{
    QVector<QVector<int>> vector = fillVector();
    if (!vector.empty())
    {
        vector = reflection(vector);    //отражение по вертикали
        int count = 0;
        for (int i = 0; i < ui->tableWidget->rowCount(); i++)
        {
            for (int j = 0; j < ui->tableWidget->columnCount(); j++)
            {
                ui->tableWidget->item(i, j)->setData(Qt::EditRole, vector[i][j]);
                count++;
            }
        }
    }
}

void MainWindow::on_pushButton_5_clicked()    //поиск элемента
{
    ui->textBrowser->clear();
    QVector<QVector<int>> vector = fillVector();
    if (!vector.empty())
    {
        QVector<QVector<int>> elementIndex = findElement(vector, ui->spinBox_3->value());    //поиск элемента
        if (elementIndex.empty())
        {
            QMessageBox::warning(nullptr, QObject::tr("Ошибка"), ("Элемент не найден  "));
        }
        else
        {
            QStringList index;
            for (int i = 0; i < elementIndex.size(); i++)
            {
                index << QString::number(elementIndex[i][0]) << "," << QString::number(elementIndex[i][1]) << "  ";
            }
            ui->textBrowser->append(index.join(""));
        }
    }
}

void MainWindow::on_pushButton_6_clicked()    //поиск трех наибольших элементов
{
    ui->textBrowser_2->clear();
    QVector<QVector<int>> vector = fillVector();
    if (!vector.empty())
    {
        if ((vector.size() * vector[0].size()) < 3)
        {
            QMessageBox::warning(nullptr, QObject::tr("Ошибка"), ("В массиве меньше трех элементов  "));
        }
        else
        {
            QVector<QVector<int>> threeMax = threeMaxElements(vector);    //поиск трех наибольших элементов
            QStringList elements;
            for (int i = 0; i < threeMax.size(); i++)
            {
                elements << QString::number(threeMax[i][0]) << "(" << QString::number(threeMax[i + 1][0]) << ","
                         << QString::number(threeMax[i + 1][1]) << ")  ";
                i++;
            }
            ui->textBrowser_2->append(elements.join(""));
        }
    }
}

void MainWindow::on_pushButton_7_clicked()    //сумма элементов главной и побочной диагоналей
{
    ui->textBrowser_3->clear();
    QVector<QVector<int>> vector = fillVector();
    if (!vector.empty())
    {
        ui->textBrowser_3->append(QString::number(diagonalSum(vector)));
    }
}

void MainWindow::on_pushButton_8_clicked()    //cумма элементов, у которых сумма индексов чётная
{
    ui->textBrowser_4->clear();
    QVector<QVector<int>> vector = fillVector();
    if (!vector.empty())
    {
        ui->textBrowser_4->append(QString::number(evenSum(vector)));
    }
}

void MainWindow::on_pushButton_9_clicked()    //создание дополнительной таблицы
{
    ui->tableWidget_2->setItemDelegate(new SpinBoxDelegate);
    ui->tableWidget_2->setRowCount(ui->spinBox_4->value());
    ui->tableWidget_2->setColumnCount(ui->spinBox_5->value());
    ui->tableWidget_2->resizeColumnsToContents();
}

void MainWindow::on_pushButton_10_clicked()    //сброс дополнительной таблицы
{
    for (int i = 0; i < ui->tableWidget_2->rowCount(); i++)    //очистка дополнительной таблицы
    {
        ui->tableWidget_2->removeRow(i);
    }
    ui->tableWidget_2->setRowCount(0);
    ui->tableWidget_2->setColumnCount(0);
}

void MainWindow::on_pushButton_11_clicked()    //заполнение случайными числами дополнительного массива
{
    if (ui->tableWidget_2->rowCount() == 0)
    {
        QMessageBox::critical(nullptr, QObject::tr("Ошибка"), ("Пустой массив  "));
    }
    else
    {
        for (int i = 0; i < ui->tableWidget_2->rowCount(); i++)
        {
            for (int j = 0; j < ui->tableWidget_2->columnCount(); j++)
            {
                QTableWidgetItem* item = new QTableWidgetItem();
                item->setData(Qt::EditRole, rand() % 2001 - 1000);
                ui->tableWidget_2->setItem(i, j, item);
            }
        }
    }
}

void MainWindow::on_pushButton_12_clicked()    //сравнение двух массивов
{
    ui->textBrowser_5->clear();
    QVector<QVector<int>> mainVector = fillVector();
    if (!mainVector.empty())
    {
        QVector<QVector<int>> addVector = fillAddVector();
        if (!addVector.empty())
        {
            if (mainVector.size() == addVector.size() && mainVector[0].size() == addVector[0].size())
            {
                QVector<QVector<int>> sameElements = compare(mainVector, addVector);    //сравнение двух массивов
                if (sameElements.empty())
                {
                    ui->textBrowser_5->append("Массивы неравны.");
                }
                else
                {
                    if (sameElements.size() == (mainVector.size() * mainVector[0].size()))
                    {
                        ui->textBrowser_5->append("Массивы равны.");
                    }
                    else
                    {
                        QStringList index;
                        for (int i = 0; i < sameElements.size(); i++)
                        {
                            index << QString::number(sameElements[i][0]) << "," << QString::number(sameElements[i][1]) << "  ";
                        }
                        ui->textBrowser_5->append("Индексы равных элементов: " + index.join(""));
                    }
                }
            }
            else
            {
                ui->textBrowser_5->append("Разное количество элементов.");
            }
        }
    }
}

void MainWindow::on_radioButton_clicked()    //сортировка по возрастанию
{
    QVector<QVector<int>> vector = fillVector();
    if (!vector.empty())
    {
        QVector<QVector<int>> sortVector = sortInAscendingOrder(vector);
        for (int i = 0; i < ui->tableWidget->rowCount(); i++)
        {
            for (int j = 0; j < ui->tableWidget->columnCount(); j++)
            {
                QTableWidgetItem* item = new QTableWidgetItem();
                item->setData(Qt::EditRole, sortVector[i][j]);
                ui->tableWidget->setItem(i, j, item);
            }
        }
    }
}

void MainWindow::on_radioButton_2_clicked()    //сортировка по убыванию
{
    QVector<QVector<int>> vector = fillVector();
    if (!vector.empty())
    {
        QVector<QVector<int>> sortVector = sortInDescendingOrder(vector);
        for (int i = 0; i < ui->tableWidget->rowCount(); i++)
        {
            for (int j = 0; j < ui->tableWidget->columnCount(); j++)
            {
                QTableWidgetItem* item = new QTableWidgetItem();
                item->setData(Qt::EditRole, sortVector[i][j]);
                ui->tableWidget->setItem(i, j, item);
            }
        }
    }
}
