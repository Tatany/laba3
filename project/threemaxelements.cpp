#include "functions.h"

QVector<QVector<int>>  threeMaxElements(QVector<QVector<int>> vector)    //поиск трех наибольших элементов
{
    QVector<QVector<int>> threeMax;

    int max1 = vector[0][0];
    QVector<int> max1index = {1, 1};
    for (int i = 0; i < vector.size(); i++)
    {
        for (int j = 0; j < vector[i].size(); j++)
        {
            if (vector[i][j] > max1)
            {
                max1 = vector[i][j];
                max1index = {i + 1, j + 1};
            }
        }
    }
    QVector<int> max1vec = {max1};
    threeMax.append(max1vec);
    threeMax.append(max1index);

    int max2 = vector[0][0];
    QVector<int> max2index = {1, 1};
    for (int i = 0; i < vector.size(); i++)
    {
        for (int j = 0; j < vector[i].size(); j++)
        {
            if (vector[i][j] > max2)
            {
                QVector<int> tmp = max2index;
                max2index = {i + 1, j + 1};
                if (max2index != max1index)
                {
                   max2 = vector[i][j];
                }
                else
                {
                    max2index = tmp;
                }
            }
        }
    }
    QVector<int> max2vec = {max2};
    threeMax.append(max2vec);
    threeMax.append(max2index);

    int max3 = vector[0][0];
    QVector<int> max3index = {1, 1};
    for (int i = 0; i < vector.size(); i++)
    {
        for (int j = 0; j < vector[i].size(); j++)
        {
            if (vector[i][j] > max3)
            {
                QVector<int> tmp = max3index;
                max3index = {i + 1, j + 1};
                if ((max3index != max1index) && (max3index != max2index))
                {
                   max3 = vector[i][j];
                }
                else
                {
                    max3index = tmp;
                }
            }
        }
    }
    QVector<int> max3vec = {max3};
    threeMax.append(max3vec);
    threeMax.append(max3index);

    return threeMax;
}
