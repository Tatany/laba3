#include "functions.h"

QVector<QVector<int>> findElement(QVector<QVector<int>> vector, int element)    //поиск элемента
{
    QVector<QVector<int>> indexElement;

    for (int i = 0; i < vector.size(); i++)
    {
        for (int j = 0; j < vector[i].size(); j++)
        {
            if (vector[i][j] == element)
            {
                indexElement.append({i + 1, j + 1});
            }
        }
    }

    return indexElement;
}
