#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QVector>

QVector<QVector<int>> compare(QVector<QVector<int>> mainVector, QVector<QVector<int>> addVector);    //сравнение двух массивов

QVector<QVector<int>> findElement(QVector<QVector<int>> vector, int element);    //поиск элемента

QVector<QVector<int>> threeMaxElements(QVector<QVector<int>> vector);    //поиск трех наибольших элементов

int diagonalSum(QVector<QVector<int>> vector);    //сумма элементов главной и побочной диагоналей

int evenSum(QVector<QVector<int>> vector);    //cумма элементов, у которых сумма индексов чётная

QVector<QVector<int>> reflection(QVector<QVector<int>> vector);    //отражение по вертикали

QVector<QVector<int>> sortInAscendingOrder(QVector<QVector<int>> vector);    //сортировка по возрастанию

QVector<QVector<int>> sortInDescendingOrder(QVector<QVector<int>> vector);    //сортировка по убыванию

#endif // FUNCTIONS_H
