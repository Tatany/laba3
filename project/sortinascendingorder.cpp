#include "functions.h"

QVector<QVector<int>> sortInAscendingOrder(QVector<QVector<int>> vector)    //сортировка по возрастанию
{
    bool sorted = false;
    while (!sorted)
    {
        sorted = true;
        int inext = 0;
        int jnext = 0;
        for (int i = 0; i < vector.size(); i++)
        {
            for (int j = 0; j < vector[0].size(); j++)
            {
                if (vector[inext][jnext] > vector[i][j])
                {
                    int tmp = vector[i][j];
                    vector[i][j] = vector[inext][jnext];
                    vector[inext][jnext] = tmp;
                    sorted = false;
                }
                inext = i;
                jnext = j;
            }
        }
    }

    return vector;
}
