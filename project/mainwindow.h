#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "delegate.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    QVector<QVector<int>> fillVector();    //заполнение массива

    QVector<QVector<int>> fillAddVector();    //заполнение дополнительного массива

    void on_pushButton_clicked();    //создание таблицы

    void on_pushButton_2_clicked();    //сброс таблицы

    void on_pushButton_3_clicked();    //заполнение случайными числами

    void on_pushButton_4_clicked();    //отражение по вертикали

    void on_pushButton_5_clicked();    //поиск элемента

    void on_pushButton_6_clicked();    //поиск трех наибольших элементов

    void on_pushButton_7_clicked();    //сумма элементов главной и побочной диагоналей

    void on_pushButton_8_clicked();    //cумма элементов, у которых сумма индексов чётная

    void on_pushButton_9_clicked();    //создание дополнительной таблицы

    void on_pushButton_10_clicked();    //сброс дополнительной таблицы

    void on_pushButton_11_clicked();    //заполнение случайными числами дополнительного массива

    void on_pushButton_12_clicked();    //сравнение двух массивов

    void on_radioButton_clicked();    //сортировка по возрастанию

    void on_radioButton_2_clicked();    //сортировка по убыванию

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
