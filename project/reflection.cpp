#include "functions.h"

QVector<QVector<int>> reflection(QVector<QVector<int>> vector)    //отражение по вертикали
{
    for (int i = 0; i < vector.size() / 2; i++)
    {
        vector[i].swap(vector[vector.size() - i - 1]);
    }

    return vector;
}
