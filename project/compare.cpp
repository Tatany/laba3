#include "functions.h"

QVector<QVector<int>> compare(QVector<QVector<int>> mainVector, QVector<QVector<int>> addVector)    //сравнение двух массивов
{
    QVector<QVector<int>> sameElements;

    for (int i = 0; i < mainVector.size(); i++)
    {
        for (int j = 0; j < mainVector[i].size(); j++)
        {
            if (mainVector[i][j] == addVector[i][j])
            {
                sameElements.append({i + 1, j + 1});
            }
        }
    }

    return sameElements;
}
